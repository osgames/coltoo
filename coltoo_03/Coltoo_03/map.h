/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: map.h
//
// VERSION	: 0.1
//
// AUTHOR	: Brett Anthoine,  November 2003
//
// CLASS	: Map
//
// DESCRIPTION	: Map of tiles
//

#ifndef MAP_H
#define MAP_H

#include <string>
#include <vector>

using namespace std;

#include "gameEngine.h"
#include "tile.h"
#include "CTileTypes.h"
#include "ColorData.h"

//forward declarations
class GameEngine;
class Tile;
class CTileTypes;
class ColorData;

class Map
{
public:
  //Constructors, destructor
  Map(GameEngine *g);
  Map(GameEngine *g,int w,int h);
  ~Map();

	void RevealAll();
	void RevealAll(Uint8 nation);
  void Reveal(int pos, Uint8 nation);
  void Reveal(int pos, Uint8 nation, int radius);

  bool   loadMap(string fileName);
  bool   Init();

  int    GetWidth()  { return width;  }
  int    GetHeight() { return height; }

  Tile*  getTile(int index)     const;
  Tile*  getTileAt(int x,int y) const;

  int    getXPosFromTile(long tile);
  int    getYPosFromTile(long tile);
  long   getTileFromPos(int x, int y);
  bool   IsOnScreen(long tile);
  
  CTileTypes *TileTypes(void);
  CTileType *TileType(Tile *t);

  void   setTile(int index,Tile* newTile);
  void   setTileAt(int x,int y,Tile* newTile);

  void   FalseTerrain(Tile *tile);
  void   PlaceRoad(Tile *tile);
  bool   ClearForest(Tile *tile);
  void   UpdateTileLinks(enum tileTypes type, int x, int y);

private:
  //-----private routines-----//
  void setArctic(int t);
  void setHighseas(int t);
  Tile *createTileFrom(char t) const;
  
  GameEngine*  m_pGame;
  int          width, height;
  vector<Tile*> tiles; //shouldn't include a fixed size
                       //do it with a vector; changed 04/07; DONE
  CTileTypes*  tileTypes;
};

#endif //MAP_H


/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: ai.h
//
// VERSION	: 0.1
//
// AUTHOR	: Goose Bonis,  November 2003
//
// CLASS	: AI
//
// DESCRIPTION	: class for AI nations and Tribes control.
//

#ifndef AI_H
#define AI_H

#include <vector>

#include "actor.h"
#include "gameEngine.h"
#include "CTileType.h"

using namespace std;

typedef vector<int> CityDistances;

class GameEngine;
class CTileType;

//changed 27/6
class AI
{
public:

//Constructor, destructor
    AI();
    AI(GameEngine*);
    virtual ~AI();

//Methods
    virtual bool Init(void);

    int Orders();
    
private:
    GameEngine *m_pGame;
    int order;
    int terrain[5][5];
    CTileType *ttypes[5][5];
    vector<CityDistances> cities;
};

#endif //AI_H


//
// $Id$
//


/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: Village.h
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Village
//
// DESCRIPTION	: Basis class for village control.
//

#ifndef VILLAGE_H
#define VILLAGE_H

#include <string>
#include <vector>
using namespace std;

#include "tile.h"

class GameEngine;
class Map;
class Tile;
class Unit;

class Village
{
public:

//Constructor, destructor
    Village();
    Village(GameEngine *g);
    virtual ~Village();
    
//GET Methods
    int ID();
    char *Name();
    char *Plural();
    long getTile();
    int MaxUnits();
    Unit *getUnit(int u);
    int getNumUnits();
    
//SET Methods
    void ID(int id);
    void Name(const char *n);
    void Plural(const char *p);
    void setTile(long t);
    void MaxUnits(int mu);
    
//ADD Methods
    void addUnit(Unit *u);
    
//INIT Methods
    void InitUnits(Uint8 nativeID, int techLevel);
    
//DESTROY Methods
    void destroyUnit(int unitID);
    void deleteUnits();
    
private:
    GameEngine *m_pGame;
    int villageID;
    string villageName;
    string villagePlural;
    long villageTile;
    int maxUnits;
    vector<Unit*> units;
};
#endif

/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "graphicsEngine.h"
#include "map.h"
#define BLINK_SPEED 432
#define SLIDE_STEPS 25

/*---------------------------DrawScene----------------------------*/

void GraphicsEngine::DrawScene(void)
{
    GlobalData *data = m_pGame->GetData();
    Map *map = m_pGame->GetMap();

    long mapnumold;
    
    /*Replaces image of unit with stored background image*/    
    DrawSURF(screen, bck1, data->stile[data->oldloc][XPOS],
             data->stile[data->oldloc][YPOS] - data->yextra - 5);

    /*Checks to see if the unit moved*/  
    if(data->flag & data->NoMoveFlag)
    {
        data->flag &= ~data->NoMoveFlag;
    }
    else 
    {
        //added 20/6
        #ifdef DEBUG
        cout << "Unit " << data->unitList[data->turn]->getName() << " ";
        #endif
        
        Tile *tile = map->getTile(data->unitList[data->turn]->getTile());
        
        #ifdef DEBUG
        cout << "is on terrain " << tile->getTerrain() << " ";
        #endif
        
        CTileType *tiletype = map->TileType(tile);
        
        #ifdef DEBUG
        if(tiletype)
            cout << "(" << tiletype->TileName() << ")";
        cout << endl << "Revealing terrain" << endl;
        #endif
        
        Reveal();
        
        #ifdef DEBUG
        cout << "Getting Terrain Movement Cost" << endl;
        if(tiletype)
            cout << "Terrain: " << tiletype->TileName() << endl;
        #endif
        
        //changed 20/6
        if(tiletype)
            data->mf -= tiletype->Movement();
        else
            data->mf--;

        #ifdef DEBUG
        cout << "MF: " << data->mf << endl;
        #endif
        
        if(data->mf <= 0)
            data->mf = 0;
        FillRect(screen, 979, 384, 30, 12, 48, 48, 48);
        
        // update movepoints left
        drawString2(screen, data->SoLfont1[3], 984, 386, "%i/%i", data->mf, data->tmf);
        
        SlideUnit(data->unitList[data->turn], data->scrnum, data->oldloc);
        Reveal();
    }

/* put new background image in bck1*/
    DrawTILE(bck1, screen, 0, 0, data->tw, data->th, data->stile[data->scrnum][XPOS],
          data->stile[data->scrnum][YPOS] - data->yextra - 5);
    data->oldloc = data->scrnum;

    if(!(data->unitList[data->turn]->isOnboard()))
    {
     UnitTerrainCheck(data->unitList[data->turn]);
    }   

    RedrawScreen(data->stile[data->scrnum][XPOS],
          data->stile[data->scrnum][YPOS] - data->yextra - 5, data->tw, data->th);

/*  Check to see if the tile has a Rumor on it
If so, call the MessageWindow and then RemoveRumor*/
    if(map->getTile(data->mapnum)->Is(TILE_RUMOR))
    {
      MessageWindow(MSG_RUMOR);
      RemoveRumor();
    }

    /*if unit disembarks on a rumor*/  
    if(data->RumorFlag)
    {
      mapnumold = data->mapnum;
      data->mapnum = data->RumorFlag;
      MessageWindow(MSG_RUMOR);
      map->getTile(data->mapnum)->setFlags(TILE_RUMOR, false);
      
      SetTerrain();
      RedrawScreen();
      
      data->mapnum = mapnumold;
      data->RumorFlag = 0;
    }

    /*if the unit tried to move onto a village*/  
    if(data->flag & data->VillageFlag)
    {
      data->flag &= ~data->VillageFlag;
      MessageWindow(MSG_VILLAGE);
      
      // update movepoints
      // TODO: maybe make it a function?
      data->mf--;
      FillRect(screen, 979, 384, 30, 12, 48, 48, 48);
      drawString2(screen, data->SoLfont1[3], 984, 386, "%i/%i", data->mf, data->tmf);
      RedrawScreen(979, 384, 30, 12);        
    }
}

/*---------------------------Reveal------------------------------*/

void GraphicsEngine::Reveal(void)
{
  /*Test to see if mapnum is legit (0-29999)
    Check to see if maptile is either to the left or right of the
    unit's position (no 'wrap around')
    Then, if the tile was shrouded it becomes partially shrouded
    If partially shrouded it becomes revealed*/

    GlobalData* data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();
    
    if(data->mapnum < 0 || data->mapnum > data->mapmax)
    {
        #ifdef DEBUG
        cerr << "invalid mapnum in Reveal(), this is possibly a bug..." << endl;
        #endif
        return;
    }
    
    // This is now just a placeholder calling Reveal() with the radius 2
    //Reveal(map->getTile(data->mapnum)->getXPos(),
    //       map->getTile(data->mapnum)->getYPos(), 2);
    map->Reveal(data->mapnum, data->nationTurn);
}

/*---------------------------Reveal------------------------------*/
void GraphicsEngine::Reveal(int x, int y, int radius)
{
/*  Check to see if maptile is either to the left or right of the 
    unit's position (no 'wrap around')
    Then, if the tile was shrouded it becomes partially shrouded
    If partially shrouded it becomes revealed*/
    GlobalData *data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();
  
  // Test to see if coordinates are legit
    if(x < 0 || x >= map->GetWidth() || y < 0 || y >= map->GetHeight())
        return;
    
    Uint8 n = data->nationTurn;
     
    #ifdef DEBUG
    cout << "Nation " << static_cast<int>(n) << endl;
    #endif
  
    for(int xx = x - radius, ii = 0; ii <= 2 * radius; xx++, ii++)
    {
        // Check for map edges
        if(xx >= 0 && xx < map->GetWidth())
        {
            for(int yy = y - radius, jj = 0; jj <= 2 * radius; yy++, jj++)
            {
                if(yy >= 0 && yy < map->GetHeight())
                {
                    switch(map->getTileAt(xx, yy)->getShrdstate(n))
                    {
                    case SHROUD_FULL:
                      if(ii > radius - 2 && ii < radius + 2 &&
                         jj > radius - 2 && jj < radius + 2) // if close enough
                          map->getTileAt(xx,yy)->setShrdstate(n, SHROUD_NONE);
                      else
                          map->getTileAt(xx,yy)->setShrdstate(n, SHROUD_PART);
                      break;
                    case SHROUD_PART:
                      if(ii > radius - 2 && ii < radius + 2 &&
                         jj > radius - 2 && jj < radius + 2) // if close enough
                          map->getTileAt(xx,yy)->setShrdstate(n, SHROUD_NONE);
/*                    else  // unnecessary, shroudstate unchanged anyway
                        map->getTileAt(xx,yy)->setShrdstate(n, 1); */
                      break;
                    default:
                      break;
                    }
                }
            }
        }
    }
}

/*-----------------------BlinkUnit---------------------------*/
void GraphicsEngine::BlinkUnit(Unit *theUnit, long &LastUpdate, bool &blink, bool &a2)
{
  GlobalData *data = m_pGame->GetData();
  Map *map = m_pGame->GetMap();
  
  if(!data->visible || data->scrndsply != MAINSCREEN || data->zl) return;
  if(LastUpdate + BLINK_SPEED < SDL_GetTicks())
  {
    if(blink)
    {
      int start = ((data->mapnum-data->start)/map->GetWidth());
      if(!start)
      {
        DrawTILE(screen,bck1,
                 data->stile[data->scrnum][XPOS],
                 data->stile[data->scrnum][YPOS],
                 data->tw,data->th-data->yextra-5,
                 0,data->yextra+5);
      }
      else /**/
      {
        DrawSURF(screen,bck1,data->stile[data->scrnum][XPOS],
                 data->stile[data->scrnum][YPOS]-data->yextra-5);
      }
    }
    else
    {
      UnitTerrainCheck(data->unitList[data->turn]);
    }
    blink = !blink;
    LastUpdate = SDL_GetTicks();
		RedrawScreen(data->stile[data->scrnum][XPOS],
                 data->stile[data->scrnum][YPOS]-data->yextra,
                 data->tw, data->th);
  }

/*  if(a2!=blink)
  {
		UpdateScreen(data->stile[data->scrnum][XPOS],
                  data->stile[data->scrnum][YPOS]-data->yextra,
                  data->tw,data->th);
    a2=blink;
  } */
}

void GraphicsEngine::SlideUnit(Unit *unit, short newloc, short oldloc)
{
    GlobalData *data = m_pGame->GetData();
    int dx, dy, x, y, x1, y1;
    short pos = unit->isBoat() ? 1 : 2;
    bool box = false; // nation box position
    int icon = unit->getGraphic();

    // does the screen need to be recentered?
    if(data->flag & data->ReCenFlag)
    {
        data->flag &= ~data->ReCenFlag;
    }
    else /**/
    {
        #ifdef DEBUG
        cout<<"Sliding Unit "<<unit->getName()<<endl;
        #endif
        // determine slide increment
        dx = (data->stile[newloc][XPOS] - data->stile[oldloc][0]) / SLIDE_STEPS;
        dy = (data->stile[newloc][YPOS] - data->stile[oldloc][1]) / SLIDE_STEPS;
        x = data->stile[oldloc][0];
        y = data->stile[oldloc][1] - data->yextra - 5;
    
        // if move is north and by mouse draw 'move to' box        
        if(data->stile[newloc][YPOS] - data->stile[oldloc][1] == -data->th
           && data->mouseClick)
            box = true;
            
        // save the original background
        DrawTILE(bck1, screen, 0, 0, data->tw, data->th, x, y);

        // slide
        for(int i = 0; i < SLIDE_STEPS; i++)
        {
            DrawSURF(screen, bck1, x, y);
    
            x += dx;
            y += dy;
            DrawTILE(bck1, screen, 0, 0, data->tw, data->th, x, y);
            
            if(newloc < 11)
            {
                NationBox(x, y+data->yextra+5,
                 unit->getNation(), pos,
                 unit->getOrders());
                DrawTILE(screen, uniticons, x, y + data->yextra + 5,
                  data->tw, data->th - data->yextra - 5, data->tile50[icon][XPOS],
                  data->tile50[icon][YPOS]);
            }
            else
            {
                NationBox(x, y, unit->getNation(), pos,
                 unit->getOrders());
                DrawTILE(screen, uniticons, x, y, data->tw, data->th,
                      data->tile50[icon][XPOS], data->tile50[icon][YPOS]);
                if(box)
                {
                    DrawTILE(screen, tilesheet1, data->stile[newloc][XPOS],
                        data->stile[newloc][YPOS], data->tw,data->th,
                        data->tilex[10], data->tiley[3]);
                }
            }
            
            if(data->stile[oldloc][XPOS] < data->stile[newloc][XPOS])
                x1 = x - dx;
            else
                x1 = x + dx;
            
            if(data->stile[oldloc][YPOS] < data->stile[newloc][YPOS])
                y1 = y - dy;
            else
                y1 = y + dy;
                
            // make sure the new unit location is drawn
            RedrawScreen(x1, y1, data->tw + dx, data->th + dy + data->yextra + 5);
            SDL_Delay(5);
        }
    }
    SetTerrain();         // draw new screen
    RedrawScreen();
    DrawTILE(bck1, screen, 0, 0, data->tw, data->th,
             data->stile[oldloc][0],
             data->stile[oldloc][1] - data->yextra - 5); //added 26/6; restore unit bck1
}

//
// $Id$
//

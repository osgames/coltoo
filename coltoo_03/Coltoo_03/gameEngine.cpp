/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: gameEngine.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Goose Bonis,  November 2003
//
// CLASS	: GameEngine
//
// DESCRIPTION	: Main class for overall game control.
//



#include <ctime>

#include "gameEngine.h"
#include "map.h"
#include "graphicsEngine.h"
#include "eventHandler.h"

//Constructor, destructor---------------------------------------------------------------//
GameEngine::GameEngine()
{
  m_pData      = NULL;
  m_pKeyb      = NULL;
  m_pSound     = NULL;
  m_pGfxEngine = NULL;
  m_pMap       = NULL;
  m_pOptions   = NULL;
}

GameEngine::~GameEngine()
{
  #ifdef DEBUG
  cout<<"----------------"<<endl;
  cout<<"Game Ended"<<endl;
  #endif
  if(m_pData)
  {
    #ifdef DEBUG
    cout<<"deleting Global Data"<<endl;
    #endif
    delete m_pData;
  }
  if(m_pKeyb)
  {
    #ifdef DEBUG
    cout<<"deleting Keyboard system"<<endl;
    #endif
    delete m_pKeyb;
  }
  if(m_pSound)
  {
    #ifdef DEBUG
    cout<<"deleting Sound system"<<endl;
    #endif
    delete m_pSound;
  }
  if(m_pGfxEngine)
  {
    #ifdef DEBUG
    cout<<"deleting Graphic Engine"<<endl;
    #endif
    delete m_pGfxEngine;
  }
  if(m_pMap)
  {
    #ifdef DEBUG
    cout<<"deleting Map"<<endl;
    #endif
    delete m_pMap;
  }
  if(m_pOptions)
  {
    #ifdef DEBUG
    cout<<"deleting Options"<<endl;
    #endif
    delete m_pOptions;
  }

  #ifdef DEBUG
  cout<<"Closing Game"<<endl;
  #endif
}

//Methods---------------------------------------------------------------------------------//
bool GameEngine::Init(int argc, char * argv[])
{
  bool bSuccess = true;

  srand(time(NULL));     //random number seed

  #ifdef DEBUG
  cout<<"Creating GameEngine {"<<endl;
  #endif
  m_pGfxEngine = new GraphicsEngine(this);
  if(m_pGfxEngine)
  {
    #ifdef DEBUG
    cout<<"  Creating Global Options"<<endl;
    #endif
    m_pOptions = new OptionList();
    #ifdef DEBUG
    cout<<"  Options: ("<<m_pOptions->Opening()<<","<<m_pOptions->Splash()<<")"<<endl;
    #endif
    if(m_pOptions) bSuccess = true;
    else bSuccess = false;
  }
  else bSuccess = false;

  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Creating Global Data"<<endl;
    #endif
    m_pData = new GlobalData (this);
    if(m_pData) bSuccess &= m_pData->Init();
    else bSuccess = false;
  }
    
  if(bSuccess) bSuccess &= m_pGfxEngine->Init();
  else bSuccess = false;

  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Splash screen {"<<endl;
    #endif
    bSuccess &= m_pGfxEngine->splash();
    #ifdef DEBUG
    cout<<"  } Splash"<<endl;
    #endif
  }

  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Creating Map"<<endl;
    #endif
    m_pMap = new Map(this, m_pOptions->MapWidth(), m_pOptions->MapHeight());
    if(m_pMap)
    {
      //should read map size form map file
      if(m_pMap->loadMap(m_pOptions->MapName())) bSuccess &= m_pMap->Init();
      else bSuccess = false;
    }
  }

  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Creating Natives {"<<endl;
    #endif
    m_pData->InitNatives();
    #ifdef DEBUG
    cout<<"  }"<<endl;
    #endif
  }
    
  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Creating keyboard input"<<endl;
    #endif
    m_pKeyb = new EventHandler();
    if(m_pKeyb) bSuccess &= m_pKeyb->Init();
    else bSuccess = false;
  }

  if(bSuccess)
  {
    #ifdef DEBUG
    cout<<"  Creating Sound system"<<endl;
    #endif
    m_pSound = new EventHandler();
    if(m_pSound) bSuccess &= m_pSound->Init();
    else bSuccess = false;
  }

  #ifdef DEBUG
  cout<<"} GameEngine"<<endl;
  #endif
  return bSuccess;
}

//                //
//Method GameLoop //---------------------------------------------------------------------//
//                //
bool GameEngine::GameLoop(void)
{

  int a,i,ii,done=0;
  static int season = 0;
  short radioold;

  m_pData->mouseClick=0;

  #ifdef DEBUG
  cout<<"Placing new Units"<<endl;
  #endif
  Unit *s, *u;
  m_pData->nation = NATION_FR;
    
  m_pData->nationTurn = NATION_SPAN;
  s = PlaceNewUnit(IDU_CARAVEL,     NATION_SPAN);
  u = PlaceNewUnit(IDU_SCOUT,       NATION_SPAN);
  u->setSkill(FindSkill(IDS_SCOUT));
  UnitBoardShip(u, s);
    
  m_pData->nationTurn = m_pData->nation;
  s = PlaceNewUnit(IDU_CARAVEL,     m_pData->nation);
  u = PlaceNewUnit(IDU_SOLDIER,     m_pData->nation);
  u->setSkill(FindSkill(IDS_DISTILLER));
  UnitBoardShip(u, s);
  u = PlaceNewUnit(IDU_COLONIST,    m_pData->nation);
  u->setSkill(FindSkill(IDS_SUGARFMR));
  UnitBoardShip(u, s);
    
  m_pData->nationTurn = NATION_ENG;
  s = PlaceNewUnit(IDU_CARAVEL, NATION_ENG);
  u = PlaceNewUnit(IDU_PIONEER, NATION_ENG);
  UnitBoardShip(u, s);
  s = PlaceNewUnit(IDU_CARAVEL, NATION_ENG);
  u = PlaceNewUnit(IDU_SOLDIER, NATION_ENG);
  UnitBoardShip(u, s);
    
  m_pData->nationTurn = NATION_DUT;
  s = PlaceNewUnit(IDU_MERCHANTMAN, NATION_DUT);
  u = PlaceNewUnit(IDU_PIONEER, NATION_DUT);
  UnitBoardShip(u, s);
  u = PlaceNewUnit(IDU_SOLDIER, NATION_DUT);
  UnitBoardShip(u, s);
  s = PlaceNewUnit(IDU_PRIVATEER, NATION_DUT);

  m_pData->recruitcost = (rand() % 200) + 200;
  m_pData->recruitcostold = m_pData->recruitcost;

  if(m_pGfxEngine->drawBackground() == false) return false;

  //expose the tiles around the 2 units
  for(m_pData->turn = 0;m_pData->turn < m_pData->unitList.size(); m_pData->turn++)
  {
    m_pData->nationTurn = m_pData->unitList[m_pData->turn]->getNation(); //added 17/6
    m_pData->mapnum = m_pData->unitList[m_pData->turn]->getTile();
    m_pMap->Reveal(m_pData->mapnum, m_pData->nationTurn);
  }

  #ifdef DEBUG
  cout<<"Starting Game..."<<endl;
  cout<<"----------------"<<endl;
  #endif
  //start the loop
  while(!done)
  {
    //added 09/07; reset data->flag
    m_pData->flag = 0;
    
    //changed 04/07; using data->getYear() instead of var year
    //year update
    m_pGfxEngine->FillRect(m_pGfxEngine->screen,635,3,85,13,48,48,48);
        
    if(season == 0)
    {
      #ifdef DEBUG
      cout<<"Spring "<<m_pData->getYear()<<endl;
      #endif
      m_pGfxEngine->drawString2(m_pGfxEngine->screen, m_pData->SoLfont1[3],
                                641, 5, "Spring %i", m_pData->getYear());
    }
    else
    {
      #ifdef DEBUG
      cout<<"Autumn "<<m_pData->getYear()<<endl;
      #endif
      m_pGfxEngine->drawString2(m_pGfxEngine->screen, m_pData->SoLfont1[3],
                                641, 5, "Autumn %i", m_pData->getYear());
    }
        
    m_pData->turnnum++;
        
    if(m_pData->getYear() >= 1500 && season == 0)
    {
      season = 1;
    }
    else
    {
      season = 0;
    }

    #ifdef DEBUG
    cout<<"Set up new recruits and cost"<<endl;
    #endif
    //set up the new recruits and cost
    m_pData->recruitcost-=(rand()%20)+15;
    m_pGfxEngine->HCSDRecruitHL(0);
    m_pGfxEngine->DrawSURF(m_pGfxEngine->hcs1,m_pGfxEngine->HCSWin[1],640,244);

    a = 1;
    #ifdef DEBUG
    cout<<"rotating between Nations"<<endl;
    #endif
    //rotate betwwen scout & ship and natives
    for(i=0; i<m_pData->playerlist.size(); i++)
    {
      m_pData->nationTurn = i; //added 17/6; Nation being played
        
      if(m_pData->getYear() == 1500 && season == 1 && m_pData->turn == 0)
      {
        m_pGfxEngine->MessageWindow(MSG_TIMESCALE);
      }
        
      #ifdef DEBUG
      cout<<"Player "<<i<<" ("<<m_pData->playerlist[i]->unitList.size()<<" units)"<<endl;
      #endif
      for(ii = 0; ii < m_pData->playerlist[i]->unitList.size(); ii++)
      {
        // if the cost of new recruit is less than 100
        //   put it on the docks
        if(m_pData->recruitcost<=100)
        {
          m_pGfxEngine->MessageWindow(MSG_NEWRECRUIT);
          radioold=m_pData->HCSradio1;
          m_pData->HCSradio1=0;
          m_pGfxEngine->HCSMouseCheck(660,244+91+8,1);  //press recruit button
          m_pData->HCSradio1=radioold;
        }
                
        if(i == m_pData->nation)
          a &= workPlayer(i, ii, done);
        else
          workPlayer(i, ii, done);
                    
        if(done) break;
      }
      if(done) break;
    }
    //added 6/2
    //checks if native units are visible
    //if native moves and is visible show slide
    if(!done)
    {
      workIndian();
        
      if(a) m_pGfxEngine->MessageWindow(MSG_ENDOFTURN);
    }
    #ifdef DEBUG
    cout<<"Done: "<<done<<endl;
    #endif
    //added 04/07; year increment should happen in the end of the turn
    if(m_pData->getYear()<1500 || season==1)
      m_pData->nextYear();
  }
	return true;
}

bool GameEngine::workPlayer(Uint8 nation, int unit, int& done)
{
  int x,y;
	//TODO: fix type of Speed and LastUpdate
  short Speed = 432;
  long LastUpdate = 0;
  bool blink = false, a2 = false;
  int unitID = m_pData->playerlist[nation]->unitList[unit]->getUnitID();

  #ifdef DEBUG
  cout<<"Woking Player "<<(int)nation<<endl;
  #endif
  //if the scout is active and loaded on ship do nothing
  m_pData->turn = unitID;
  if(m_pData->unitList[unitID]->isOnboard() || m_pData->unitList[unitID]->getOrders() != ORD_NONE)
  {
    return 1;
  }
  //otherwise
  else
  {
    m_pData->unitList[unitID]->setMovesLeft(m_pData->unitList[unitID]->getMoveCapability());
    m_pData->mf = m_pData->unitList[unitID]->getMovesLeft();
    m_pData->tmf = m_pData->mf;
    m_pData->mapnum=m_pData->unitList[unitID]->getTile();
    m_pData->start=m_pData->unitList[unitID]->getStartNumber();

    //changed 04/07; not in use
    //m_pMap->getTile(m_pData->mapnum)->setFlags(TILE_UNIT, true);

    m_pGfxEngine->ReCenter();
    m_pData->flag&=~m_pData->ReCenFlag;
    m_pGfxEngine->Reveal();      //removes, or partially removes, the shroud
    m_pGfxEngine->SetTerrain();
    m_pGfxEngine->DrawTILE(m_pGfxEngine->bck1,m_pGfxEngine->screen,
                           0,0,m_pData->tw,m_pData->th,
                           m_pData->stile[m_pData->scrnum][0],
                           m_pData->stile[m_pData->scrnum][1]-m_pData->yextra-5);
    m_pData->oldloc=m_pData->scrnum;
                
    m_pGfxEngine->UpdateUnitInfo(m_pData->unitList[unitID]);

    m_pGfxEngine->FillRect(m_pGfxEngine->screen,979,384,30,12,48,48,48);
    m_pGfxEngine->drawString2(m_pGfxEngine->screen, m_pData->SoLfont1[3],
                              984, 386, "%i/%i", m_pData->mf, m_pData->tmf);

    x = m_pData->tile26[m_pData->unitList[unitID]->getGSmall()][0];
    y = m_pData->tile26[m_pData->unitList[unitID]->getGSmall()][1];

    //draw 'mini' unit in information window
    m_pGfxEngine->DrawTILE(m_pGfxEngine->activebck,m_pGfxEngine->screen,
                           0,0,32,25,979,351);
    m_pGfxEngine->DrawTILE(m_pGfxEngine->screen,m_pGfxEngine->unitsS,
                           979,351,32,25,x,y);

    m_pGfxEngine->RedrawScreen();
    m_pGfxEngine->UnitTerrainCheck(m_pData->unitList[unitID]);   //Water or Land?
    m_pGfxEngine->RedrawScreen(m_pData->stile[m_pData->scrnum][0],
                               m_pData->stile[m_pData->scrnum][1] -
                               m_pData->yextra-5,m_pData->tw,m_pData->th);
        
    LastUpdate = SDL_GetTicks();

    while(m_pData->mf>0)
    {
      //not sure if these next two lines are really needed
      m_pData->event.type=0;
      m_pData->event.button.button=0;
      m_pData->event.button.type=0;

      ///check for click or key press or mouse motion
      SDL_SetTimer(50, EventHandler::FireTimedEvent);
      SDL_WaitEvent(&m_pData->event);
      SDL_SetTimer(0, NULL);
            
      if(m_pData->event.type == SDL_ACTIVEEVENT)
      {
        if(!m_pData->event.active.gain)
          SDL_WaitEvent(&m_pData->event);
        LastUpdate = SDL_GetTicks();
        m_pGfxEngine->RedrawScreen();
      }

      //if mouse moved on the HCS (Home Country Screen see if we have a
      // 'mouse over' event
      if(m_pData->event.type == SDL_MOUSEMOTION && m_pData->scrndsply == 2)
      {
        m_pGfxEngine->HCSMouseOverSort();
      }

      //blink the active unit
      m_pGfxEngine->BlinkUnit(m_pData->unitList[unitID],LastUpdate,blink,a2);

      //see if the ESCAPE key was pressed.  if so end program
      if ( m_pData->event.type == SDL_QUIT )
      {
        #ifdef DEBUG
        cout<<"Quiting"<<endl;
        #endif
        done = 1;
        m_pData->mf = 0;
      }

      //handle a click
      if (m_pData->event.button.type==SDL_MOUSEBUTTONDOWN)
      {
        if(m_pGfxEngine->MouseCheck())
        {
          #ifdef DEBUG
          cout<<"Clicked 'Quit'"<<endl;
          #endif
          done=1;
          m_pData->mf=0;
        }
        continue;
      }

      //handle a key press
      if ( m_pData->event.type == SDL_KEYDOWN )
      {
        if(m_pGfxEngine->KeyBoard(m_pData->event.key.keysym.sym))
        {
          done=1;
          m_pData->mf=0;
        }
      }
      if(m_pData->unitList[unitID]->getOrders()!=ORD_NONE) m_pData->mf = 0;
    }

    //if quit button was clicked, or 'Q' or 'q' key, end program
    if(done)
    {
      #ifdef DEBUG
      cout<<"All Done"<<endl;
      #endif
      return true;
    }

    //store the postition and screen information of the unit at the
    // end of its turn
    m_pData->unitList[unitID]->setTile(m_pData->mapnum);
    m_pData->unitList[unitID]->setStartNumber(m_pData->start);

    //erase the mini unit
    m_pGfxEngine->DrawSURF(m_pGfxEngine->screen,m_pGfxEngine->activebck,979,351);

    //pause a half second so the player can see last move of unit
    SDL_Delay(500);
  }
  return false;
}

//changed 30/6; using new Indians class
void GameEngine::workIndian(void)
{
  int x, y, i, ii, k, dx, dy, steps=25, x1, y1;
  long mapnumold;
  int ind, v;

  if(!m_pData->indians) return;
  for(ind=0; ind<m_pData->indians->getNumIndians(); ind++)
  {
    Indian *I = m_pData->indians->getIndian(ind);
    if(!I) continue;
    #ifdef DEBUG
    cout<<"Native "<<ind<<" ("<<I->getNumVillages()<<" villages)"<<endl;
    #endif
    AI *ai = I->getAI();
    for(v=0; v<I->getNumVillages(); v++)
    {
      Village *V = I->getVillage(v);
      if(!V) continue;
      for(i=0; i<V->getNumUnits(); i++)
      {
        Unit *u = V->getUnit(i);
        if(!u) continue;
        if(ai)
        {
                        
        }
        else
        {
          u->setMovesLeft(u->getMoveCapability());
          m_pData->mf = u->getMovesLeft();
          while(m_pData->mf>0)
          {
            x = (rand()%3) - 1;
            y = (rand()%3) - 1;
            if(x || y)
            {
              m_pData->mapnum = u->getTile();
              m_pData->start=u->getStartNumber();
              mapnumold = m_pData->mapnum;
              m_pGfxEngine->MoveUnit(u, x, y);
              #ifdef DEBUG
              cout<<"Moving Native "<<u->getName()<<endl;
              #endif
              if(m_pMap->getTile(mapnumold)->getShrdstate(m_pData->nationTurn)!=SHROUD_FULL &&
                 m_pMap->getTile(m_pData->mapnum)->getShrdstate(m_pData->nationTurn)!=SHROUD_FULL)
              {
                #ifdef DEBUG
                cout<<"Unit is visible"<<endl;
                #endif
                if(!m_pMap->IsOnScreen(mapnumold))
                {
                  #ifdef DEBUG
                  cout<<"Centering on Native "<<u->getName()<<endl;
                  #endif
                  m_pGfxEngine->ReCenter();
                  m_pData->flag |= m_pData->ReCenFlag;
                }
                if(m_pData->flag&m_pData->ReCenFlag)
                {
                  m_pGfxEngine->SetTerrain();
                  m_pGfxEngine->UnitTerrainCheck(u);              /*Water or Land?*/
                  m_pGfxEngine->UpdateScreen(5,25,830,725);
                }
                m_pData->flag &= ~m_pData->ReCenFlag;
                if(mapnumold!=m_pData->mapnum)
                {
                  #ifdef DEBUG
                  cout<<"Moving from "<<mapnumold<<" to "<<m_pData->mapnum<<endl;
                  #endif
                  int strnum = m_pMap->getTile(mapnumold)->getScreenTile();
                  int endnum = m_pMap->getTile(m_pData->mapnum)->getScreenTile();
                  #ifdef DEBUG
                  cout<<"Native placement: "<<strnum<<" --> "<<endnum<<" ("<<m_pGfxEngine->GetScreenWidth()<<"x"<<m_pGfxEngine->GetScreenHeight()<<")"<<endl;
                  #endif
                  if(strnum>0 && endnum>0)
                  {
                    m_pGfxEngine->SlideUnit(u, endnum,strnum);
                    //m_pGfxEngine->UpdateScreen(5,25,830,725);
                    //SDL_Delay(500); //uncomment to see unit move
                  }
                }
              }
            }
            #ifdef DEBUG
            cout<<"Setting the Movement"<<endl;
            #endif
            if(m_pMap->TileType(m_pMap->getTile(m_pData->mapnum)))
              m_pData->mf -= m_pMap->TileType(m_pMap->getTile(m_pData->mapnum))->Movement();
            else m_pData->mf--;
          }
        }
      }
      //SDL_Delay(500); //uncomment to see village delay
    }
  }
  m_pGfxEngine->SetTerrain();
  m_pData->flag &= ~m_pData->VillageFlag;
}

void GameEngine::UpdateUnitMove(int mapnum, int steps)
{
  int x, y, dx, dy, ii, x1, y1;
    
  if(m_pData->flag & m_pData->NoMoveFlag)
  {
    m_pData->flag &= ~m_pData->NoMoveFlag;
  }
  else
  {
    if(m_pMap->getTile(m_pData->mapnum)->getShrdstate(m_pData->nationTurn)<2 ||
       m_pMap->getTile(mapnum)->getShrdstate(m_pData->nationTurn)<2)
    {
      m_pGfxEngine->SetTerrain();
      m_pGfxEngine->RedrawScreen();
            
      m_pGfxEngine->DrawTILE(m_pGfxEngine->bck1,m_pGfxEngine->screen,0,0,
                             m_pData->tw,m_pData->th,
                             m_pData->stile[m_pData->oldloc][XPOS],
                             m_pData->stile[m_pData->oldloc][YPOS]-m_pData->yextra-5);
                  
      dx=(m_pData->stile[m_pData->scrnum][XPOS]-m_pData->stile[m_pData->oldloc][0])/steps;
      dy=(m_pData->stile[m_pData->scrnum][YPOS]-m_pData->stile[m_pData->oldloc][1])/steps;
      x=m_pData->stile[m_pData->oldloc][0];
      y=m_pData->stile[m_pData->oldloc][1]-m_pData->yextra-5;
        
      for(ii=0;ii<steps;ii++)
      {
        m_pGfxEngine->DrawSURF(m_pGfxEngine->screen,m_pGfxEngine->bck1,x,y);
        x+=dx;
        y+=dy;
        m_pGfxEngine->DrawTILE(m_pGfxEngine->bck1,m_pGfxEngine->screen,0,0,
                               m_pData->tw,m_pData->th,x,y);
              
        if(m_pData->scrnum<11)
        {
          m_pGfxEngine->NationBox(x, y+m_pData->yextra+5, NATION_TRIBE, 3, ORD_NONE);
          m_pGfxEngine->DrawTILE(m_pGfxEngine->screen,m_pGfxEngine->tilesheet1,
                                 x,y+m_pData->yextra+5,
                                 m_pData->tw,m_pData->th-m_pData->yextra-5,
                                 m_pData->tilex[4],
                                 m_pData->tiley[2]+m_pData->yextra+5);
        }
        else
        {
          m_pGfxEngine->NationBox(x, y, NATION_TRIBE, 3, ORD_NONE);
          m_pGfxEngine->DrawTILE(m_pGfxEngine->screen,m_pGfxEngine->tilesheet1,
                                 x,y,m_pData->tw,m_pData->th,
                                 m_pData->tilex[4],m_pData->tiley[2]);
        }
              
        if(m_pData->stile[m_pData->oldloc][0]<m_pData->stile[m_pData->scrnum][XPOS])
          {x1=x-dx;}
        else
          {x1=x+dx;}
                      
        if(m_pData->stile[m_pData->oldloc][1]<m_pData->stile[m_pData->scrnum][YPOS])
          {y1=y-dy;}
        else
          {y1=y+dy;}
                      
        m_pGfxEngine->RedrawScreen(x1,y1,m_pData->tw+dx,
                                   m_pData->th+dy+m_pData->yextra+5);
        SDL_Delay(5);
      } /**/
    }
    else
    {
        m_pData->flag &= ~m_pData->NoMoveFlag;
    }
  }
}

Unit *GameEngine::FindUnitAt(long tile, bool isBoat)
{
  int i;
  //changed 04/07; to find if a unit is on tile use tile->findWalkingUnit()
  return m_pMap->getTile(tile)->findWalkingUnit(isBoat);
}

Unit *GameEngine::PlaceNewUnit(int unitID, int nation)
{
  m_pData->unitList.push_back(Unit::loadUnitFile(unitID));
  int i = m_pData->unitList.size() - 1;
  int sea_range = 50, sea_start = 75; //added 27/6
    
  m_pData->unitList[i]->setUnitID(i);
  m_pData->unitList[i]->setMovesLeft(m_pData->unitList[i]->getMoveCapability());
  m_pData->unitList[i]->setNation(nation);
  m_pData->nationTurn = nation; //added 17/6

  if(m_pData->unitList[i]->isBoat())
  {
    int x, y = (rand()%sea_range)+sea_start;    // Select starting point for ship
    if(y>=m_pMap->GetHeight()) y = m_pMap->GetHeight()/2;
    
    for(x = 99; x < m_pMap->GetWidth(); x++)
    {
      Tile *tile = m_pMap->getTile(m_pMap->GetWidth()*y + x);
      Unit *tmp = tile->findWalkingUnit(true);
      if(tmp)
      {
        //added 26/6; can't place a ship ontop of a foreign ship
        if(tmp->getNation()!=nation)
        {
          #ifdef DEBUG
          cout<<"Place occupied by foreign ship; trying next tile..."<<endl;
          #endif
          continue;
        }
      }
      if(tile->getTerrain() == TERRAIN_HIGHSEAS)
      {
        m_pData->unitList[i]->setTile(tile->getIndex());
        //tile->setFlags(TILE_UNIT, true);
        tile->setWalkingUnit(m_pData->unitList[i]);
        m_pData->unitList[i]->setPositionX(x);
        m_pData->unitList[i]->setPositionY(y);
        m_pData->unitList[i]->setStartNumber(tile->getIndex() - 6*m_pMap->GetWidth()-5);
        break;
      }
    }
  }
  else
  {
    int x, y;
    while(1)
    {
      y = (rand() % (m_pMap->GetHeight()-50)) + 25;    // Select starting point for scout
      x = (rand() % (m_pMap->GetWidth()-50)) + 25;
      if(y>=m_pMap->GetHeight()) y = m_pMap->GetHeight()/2;
      if(x>=m_pMap->GetWidth()) x = m_pMap->GetWidth()/2;
      Tile *tile = m_pMap->getTile((m_pMap->GetWidth()*y)+x);
            
      if(!tile->Is(TILE_RUMOR) &&
          tile->getTerrain()!=TERRAIN_OCEAN &&
          tile->getTerrain()!=TERRAIN_HIGHSEAS &&
          tile->getTerrain()!=TILE_PEAK &&
          tile->getVillages()==0 &&
          tile->getTerrain()!=TERRAIN_LAKE)
      {
        m_pData->unitList[i]->setPositionX(x); //=(m_pMap->GetWidth()*y)+x;
        m_pData->unitList[i]->setPositionY(y);
        m_pData->unitList[i]->setTile(tile->getIndex());
        tile->setWalkingUnit(m_pData->unitList[i]);
        m_pData->unitList[i]->setStartNumber(tile->getIndex() - 6*m_pMap->GetWidth()-5);
        break;
      }
    }
  }
  m_pData->playerlist[nation]->unitList.push_back(m_pData->unitList[i]);
  return m_pData->unitList[i];
}

bool GameEngine::UnitBoardShip(Unit *unit, Unit *ship)
{
  if((!unit) || (!ship)) return false;
  if(!(ship->addPassenger(unit))) return false;
  unit->setOnboardFlag(true);
  m_pMap->getTile(unit->getTile())->removeWalkingUnit(unit);
  m_pMap->getTile(ship->getTile())->setWalkingUnit(unit);
  unit->setTile(ship->getTile());
  unit->setStartNumber(ship->getStartNumber());
  return true;
}

void GameEngine::CombatAnalysis(Unit *attacker, Unit *defender)
{
  #ifdef DEBUG
  if(attacker) cout<<"Combat analysis for Nation "<<((int)m_pData->nationTurn)<<endl;
  if(defender) cout<<"Nation "<<((int)attacker->getNation())<<" is attacking Nation "<<((int)defender->getNation())<<endl;
  #endif
  if(attacker->getNation() == m_pData->nationTurn)
  {
     #ifdef DEBUG
    if(attacker) cout<<"Attacker attacking at "<<attacker->getAttack()<<endl;
    #endif
    if(!(attacker->getAttack()))
      m_pGfxEngine->MessageWindow(MSG_CANTATTACK);
    else
      m_pGfxEngine->MessageWindow(MSG_COMBAT, attacker, defender);
  }
}

Colony *GameEngine::PlaceColony(Unit *builder, long tile)
{
  Colony *col;
  int *nameID;
  string name;
  bool isPort = false;
  int x, y;
  for(y=-1; y<2; y++)
  {
    for(x=-1; x<2; x++)
    {
      Tile *t = m_pMap->getTile(tile + (y*m_pMap->GetWidth()+x));
      if(t)
      {
        if(t->Is(TILE_WATER))
        {
          isPort = true;
          break;
        }
      }
    }
    if(isPort) break;
  }
        
  col = new Colony(isPort);
  m_pMap->PlaceRoad(m_pMap->getTile(tile));
  m_pMap->getTile(tile)->setFlags(TILE_BUILD, true);
  m_pData->colonies.push_back(col);
  col->setNation(builder->getNation());
  col->setTile(tile);
    
  nameID = &(m_pData->nextColName[col->getNation()]);
  name = m_pData->colName[col->getNation()][*nameID];
  col->setName(name);
  (*nameID)++;
        
  //TODO:    builder->joinColony(col);
  return col;
}

Colony *GameEngine::FindColonyAt(long tile)
{
  int i;
  for(i = 0; i < m_pData->colonies.size(); i++)
  {
    if(m_pData->colonies[i]->getTile() == tile)
      return m_pData->colonies[i];
  }
  return NULL;
}

Colony *GameEngine::FindColonyOf(int nation)
{
  int i;
  for(i = 0; i < m_pData->colonies.size(); i++)
  {
    if(m_pData->colonies[i]->getNation() == nation)
      return m_pData->colonies[i];
  }
  return NULL;
}

Skill *GameEngine::FindSkill(int type)
{
  int i;
  for(i = 0; i < m_pData->skillList.size(); i++)
  {
    if(m_pData->skillList[i]->getType() == type)
    {
      return m_pData->skillList[i];
    }
  }
  return NULL;
}

//
// $Id$
//


/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "graphicsEngine.h"
#include "map.h"

/*---------------------------KeyBoard-----------------------------*/
int GraphicsEngine::KeyBoard(int k)
{
    GlobalData* data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();
    unsigned char move;
    int x, y;

  if(k==SDLK_ESCAPE){return 1;}
  if(k==SDLK_q){return 1;}
  
//changed 5/31
//space bar ends unit turn even if not visible  
  if(k == SDLK_SPACE && data->scrndsply == 0 && !data->zl)
  {
      data->mf = 0;
      data->visible = 1;
      return 0;
  }

  /*Checks the zoom state and if the unit is visible
    Handles keypad movement
    Handles hotkeys B,P,R,etc.*/

  if(data->zl==0&&data->visible==1&&data->scrndsply==0)
  {
  
    UnitTerrainCheck(data->unitList[data->turn]);
    UpdateScreen(data->stile[data->scrnum][XPOS],
                  data->stile[data->scrnum][YPOS]-data->yextra-5,
                  data->tw,data->th+5);
              
    switch (k)
    {
      case (SDLK_KP9):                 /*NE*/
      case (SDLK_PAGEUP):
      {
        move=1;
        x = 1; y = -1;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP6):                 /*E*/
      case (SDLK_RIGHT):
      {
        move=2;
        x = 1; y = 0;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP3):                 /*SE*/
      case (SDLK_PAGEDOWN):
      {
        move=3;
        x = 1; y = 1;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP2):                  /*S*/
      case (SDLK_DOWN):
      {
        move=4;
        x = 0; y = 1;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP1):                  /*SW*/
      case (SDLK_END):
      {
        move=5;
        x = -1; y = 1;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP4):                  /*W*/
      case (SDLK_LEFT):
      {
        move=6;
        x = -1; y = 0;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP7):                  /*NW*/
      case (SDLK_HOME):
      {
        move=7;
        x = -1; y = -1;
        data->flag|=data->MoveFlag;
        break;
      }
      case (SDLK_KP8):                  /*N*/
      case (SDLK_UP):
      {
        move=8;
        x = 0; y = -1;
        data->flag|=data->MoveFlag;
        break;
      }
      
      case (SDLK_KP5):                  /*ReCenter*/
      {
        ReCenter();
        //data->flag|=data->ReCenFlag+data->UpDateFlag;
        data->flag &= ~data->ReCenFlag; //added 08/07; screen already have been recentered
        SetTerrain();
        UnitTerrainCheck(data->unitList[data->turn]);              /*Water or Land?*/
        UpdateScreen(5,25,830,725);
        data->visible=1;
        break;
      }
      case SDLK_f:
       data->unitList[data->turn]->fortify();
       data->mf = 0;
       ScreenUpdate();
       break;
              
      case SDLK_s:
       data->unitList[data->turn]->sentry();
       data->mf = 0;
       ScreenUpdate();
       break;
              
      case (SDLK_b):                    /*colony*/
      {
        Orders(ORD_BUILD);
        break;
      }        
      case (SDLK_c):                    /*clear*/
      {
        //added 30/6; 'C' centers unit if boat (boats can't clear anything)
        if(data->unitList[data->turn]->isBoat())
        {
          ReCenter();
          //data->flag|=data->ReCenFlag+data->UpDateFlag;
          data->flag &= ~data->ReCenFlag; //added 08/07; screen has already been recentered
          SetTerrain();
          UnitTerrainCheck(data->unitList[data->turn]);              /*Water or Land?*/
          UpdateScreen(5,25,830,725);
          data->visible=1;
        }
        else
          Orders(ORD_CLEAR);
        break;
      }        
      case (SDLK_p):                    /*plow*/
      {
        #ifdef DEBUG
        cout<<"Ordering Plow"<<endl;
        #endif
        Orders(ORD_PLOW);
        break;
      }
      case (SDLK_r):                    /*road*/
      {
        Orders(ORD_ROAD);
        break;
      }
      case (SDLK_g):                    /*grid*/
      {
        SetGrid(962, 325);
        break;
      }
      case (SDLK_a):                    /*clears shroud*/
      {
        //changed 08/07; only reveal map for current Nation
        map->RevealAll(data->nationTurn);
        SetTerrain();
        UpdateScreen();
        break;
      }              
      default: 
        data->flag&=~data->MoveFlag;
    }

/*  If a key moved the unit, update the screen */    
    if(data->flag&data->MoveFlag)
    {
      //changed 30/6; using new MoveUnit
      MoveUnit(data->unitList[data->turn], x, y);
      DrawScene();
      data->flag&=~data->MoveFlag;
      return 0;
    }
  }
  
  if(k==SDLK_z&&data->scrndsply==0)
  {
    int x0 = 5, y0 = 2; //from mouse.cpp, line 46
    //added 08/08; toggle zoom button
    Buttons2(x0+380,11,y0,1,7,344,52,267,76,18); //from mouse.cpp, line 88
    ToggleZoom();
    return 0;
  }
  if(k==SDLK_a&&data->zl==1)
  {
    DisplayZoom(SMOOTHING_ON);
    UpdateScreen();
    return 0;
  }
  if(k==SDLK_s&&data->zl==1)
  {
    DisplayZoom(SMOOTHING_OFF);
    UpdateScreen();
  }

  return 0;
}


//
// $Id$
//


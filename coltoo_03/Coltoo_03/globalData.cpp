/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: globalData.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Goose Bonis,  November 2003
//
// CLASS	: GlobalData
//
// DESCRIPTION	: storage data class for global data.
//

#include "globalData.h"
#include "Indians.h"

//Constructors, destructor
GlobalData::GlobalData(GameEngine* game)
: m_pGame(game)
{
    zl=0;flag=0;scrmax=153;mapmax=29999;
    red=255;green=0;turnnum=1;x_res=1024;y_res=768;

    tilex[0] =  1; tilex[1] = 77; tilex[2] = 153; tilex[3] = 229;
    tilex[4] =305; tilex[5] =381; tilex[6] = 457; tilex[7] = 533;
    tilex[8] =609; tilex[9] =685; tilex[10]= 761; tilex[11]= 837;
    tilex[12]=913; tilex[13]=989; tilex[14]=1065; tilex[15]=1141;

    gdstilesX[0] =  1; gdstilesX[1] = 52; gdstilesX[2] =103; gdstilesX[3] =154;
    gdstilesX[4] =205; gdstilesX[5] =256; gdstilesX[6] =307; gdstilesX[7] =358;
    gdstilesX[8] =409; gdstilesX[9] =460; gdstilesX[10]=511; gdstilesX[11]=562;
    gdstilesX[12]=613; gdstilesX[13]=664; gdstilesX[14]=715; gdstilesX[15]=766;

    tiley[0]=1;tiley[1]=52;tiley[2]=103;tiley[3]=154;tiley[4]=205;tiley[5]=256;

    tiley2[0]=1; tiley2[1]=67; tiley2[2]=156; tiley2[3]=222;

    shiptypenum[0]=shiptypenum[2]=shiptypenum[3]= 0;
    shiptypenum[1]=1;
    shiptypenum[4]=shiptypenum[5]=shiptypenum[6]= 0;

    HLCheckOld[0]=HLCheckOld[1]=HLCheckOld[2]=0;

    oldloc=55;scrnum=71;tw=75;th=50;rows=200;cols=150;
    JobCount=0; visible=1; yextra=2;
    scrndsply = 0; grid = 0; HCSradio1 = 0; Pstart = 0;
    Tstart=1;shipnum=0;
      
    //changed 30/6
                    /*screen directions*/
    sNE=sdirs[2][0]=-10;sE=sdirs[2][1]=1;sSE=sdirs[2][2]=12;
    sS=sdirs[1][2]=11;
    sSW=sdirs[0][2]=10;sW=sdirs[0][1]=-1;sNW=sdirs[0][0]=-12;
    sN=sdirs[1][0]=-11;
    
    //changed 30/6
                      /*map directions*/
    mNE=mdirs[2][0]=-149;mE=mdirs[2][1]=1;mSE=mdirs[2][2]=151;
    mS=mdirs[1][2]=150;
    mSW=mdirs[0][2]=149;mW=mdirs[0][1]=-1;mNW=mdirs[0][0]=-151;
    mN=mdirs[1][0]=-150;

    gold=1000000;RumorFlag=0;

    ReCenFlag=BIT1;MoveFlag=BIT2;UpDateFlag=BIT3;
    NoMoveFlag=BIT4;VillageFlag=BIT5;SpaceFlag=BIT6;
    RoadFlag=BIT7;//horz=0;vert=1;

    counterbutton=2;counterspace=0;

    caravel=1;
	merchantman=2;
	//galleon=3;privateer=4;frigate=5;manowar=6;
    tons=100;counter=100;//nation=0;

	rHL=0;
	pHL=0;
	tHL=0;

//added 6/2
//sets number of colonies and colony names to 0
	colonyCount=0;
	colNameCount[0]=0;colNameCount[1]=0;colNameCount[2]=0;colNameCount[3]=0;
    indians = NULL;	

  //added 04/07
  m_nYear = 1942;
}

GlobalData::~GlobalData()
{
  #ifdef DEBUG
  cout<<"deleting unitlist"<<endl;
  #endif
  vector<Unit*>::iterator it = unitList.begin();
  for( it = unitList.begin(); it != unitList.end(); it++ )
  {
    if (*it) delete (*it);
  }
  unitList.clear();
  #ifdef DEBUG
  cout << "deleting playerlist" << endl;
  #endif
  playerlist.clear();
  #ifdef DEBUG
  cout<<"deleting indianlist"<<endl;
  #endif
  if(indians) delete(indians);
  indians = NULL;
}


//
//Methods
//
bool GlobalData::Init(void)
{
    char buffer[255],filename[255];
    short k,i,done=0,temp = 0,num=0,k2=0;
    unsigned int temp0,temp1,temp2,temp3,temp4,temp5;
    FILE *fp = NULL;
    
    #ifdef DEBUG
    cout<<"  Init GlobalData {"<<endl;
    #endif
    msg = new Message;
    #ifdef DEBUG
    cout<<"    Natives"<<endl;
    #endif
    nativeOnTile = new short[30000];
    memset(nextColName, 0, sizeof(nextColName));
    #ifdef DEBUG
    cout<<"    Loading skills"<<endl;
    #endif
    Skill::loadSkills(&skillList);

    #ifdef DEBUG
    cout<<"    Loading labels"<<endl;
    #endif
    strcpy(filename, "data/text/labels.txt");

    //Check to see if the file opened
    if((fp=fopen(filename, "r")) == NULL)
    {
        cout << "ERROR opening file " << filename << "\n\n" ;
        return false;
    }

    while(!feof(fp))
    {
        fgets(buffer, 255, fp);
        if(buffer[0]=='@'&&buffer[1]=='J'&&buffer[2]=='o')
        {
             while(done==0)
             {
                  fgets(Jobs[JobCount], 25, fp);
                  if(Jobs[JobCount][0]=='@'){done=1;break;}
                  JobCount++;
             }
        }

        //orders    
        if(buffer[0]=='@'&&buffer[1]=='O'&&buffer[2]=='r')
        {
             for(i=0;i<10;i++){fgets(Order[i],25,fp);}
        }

        //game  
        if(buffer[0]=='@'&&buffer[1]=='G'&&buffer[2]=='a')
        {
             for(i=0;i<9;i++){fgets(Game[i],25,fp);}       
        }

        //view
        if(buffer[0]=='@'&&buffer[1]=='V'&&buffer[2]=='i')
        {
             for(i=0;i<2;i++){fgets(View[i],25,fp);}       
        }

        //buy
        if(buffer[0]=='@'&&buffer[1]=='B'&&buffer[2]=='u')
        {
             for(i=0;i<6;i++){fgets(Buy[i],25,fp);}       
        }

        //buycost
        if(buffer[0]=='@'&&buffer[1]=='B'&&buffer[2]=='C')
        {
             for(i=0;i<6;i++){fgets(BuyCost[i],25,fp);}       
        }

        //jobcost
        if(buffer[0]=='@'&&buffer[1]=='J'&&buffer[2]=='C')
        {
             for(i=0;i<JobCount+1;i++){fgets(JobsCost[i],25,fp);}       
        }

        //labels
        if(buffer[0]=='@'&&buffer[1]=='L'&&buffer[2]=='a')
        {
             i=0;done=0;
             while(done==0)
             {
                  fgets(Labels[i], 25, fp);
                  if(Labels[i][0]=='@'){done=1;}
                  i++;
             }       
        }      
    }
    fclose(fp);    

    #ifdef DEBUG
    cout<<"    Loading TextColors"<<endl;
    #endif
    strcpy(filename,"data/text/TextColors.txt");

    if((fp = fopen(filename,"r"))==NULL)
    {
        cout << "ERROR opening file " << filename << "\n\n";
        return false;
    }

    for(i=0;i<18;i+=6)
    {
        fscanf(fp,"%lu,%lu,%lu,%lu,%lu,%lu",&temp0,&temp1,&temp2,&temp3,
                                            &temp4,&temp5);
        text1color[i]=temp0;
        text1color[i+1]=temp1;
        text1color[i+2]=temp2;

        text1color[i+3]=temp3;
        text1color[i+4]=temp4;
        text1color[i+5]=temp5;
    }

    fclose(fp);
    

    #ifdef DEBUG
    cout<<"    Loading pricelist"<<endl;
    #endif
    sprintf(filename,"data/text/pricelist.txt");

    if((fp=fopen(filename,"r"))==NULL)
    {
        cout << "ERROR opening file " << filename << "\n\n";
        return false;
    }

    for(i=0,k=0;i<32;i++)
    {
        fscanf(fp,"%d",&temp);

        if(k==0){price[num][0]=temp;k=1;}
        else{price[num][1]=temp;k=0;num++;}
        k2++;
    }

    fclose(fp);

//from initarrays1.cpp - begin
    int x, y;
    for(y=25, i=0; y<725; y+=50)
    {
        for(x=8;x<833;x+=75, i++)
        {
            stile[i][XPOS]=x;
            stile[i][YPOS]=y;
        }
    }

    for(y = 1, i = 0; y < 257; y += 51)
    {
        for(x = 1; x < 838; x += 76, i++)
        {
            tile50[i][XPOS] = x;
            tile50[i][YPOS] = y;
        }
    }

    for(y = 1, i = 0; y < 132; y += 26)
    {
        for(x = 1; x < 365; x += 33, i++)
        {
            tile26[i][XPOS] = x;
            tile26[i][YPOS] = y;
        }
    }

    #ifdef DEBUG
    cout<<"    Create the Caravel"<<endl;
    #endif
    /*put the data for the ship on the map into the shipstruct array.*/
    DefaultShipName(shiptypenum[caravel],Buy,caravel);
    strcpy(units_ships[shipnum].type,Buy[caravel]);
    units_ships[shipnum].typenum=caravel;
    ShipCharacteristics(units_ships[shipnum].typenum);

    #ifdef DEBUG
    cout<<"    Create the Merchantman"<<endl;
    #endif
    /*put the data for the ship on the map into the shipstruct array.*/
    DefaultShipName(shiptypenum[merchantman],Buy,merchantman);
    strcpy(units_ships[shipnum].type,Buy[merchantman]);
    units_ships[shipnum].typenum=merchantman;
    ShipCharacteristics(units_ships[shipnum].typenum);

    /*zero the value of the SelectedShipstart & 
      SelectedShipCargo arrays;*/
    for(i=0;i<100;i++)
    {
        SSstart[i]=0;
        SSCstart[i]=0;
    }

//added 6/2
//reads colony names
    #ifdef DEBUG
    cout<<"    Loading Colony data"<<endl;
    #endif
    strcpy(filename, "data/text/colony.txt");

    //Check to see if the file opened
    if((fp=fopen(filename, "r")) == NULL)
    {
        cout << "ERROR opening file " << filename << "\n\n" ;
        return false;
    }

    while(!feof(fp))
    {
        fgets(buffer, 255, fp);
        done=0;
        if(buffer[0]=='@'&&buffer[1]=='E'&&buffer[2]=='N')
        {
             while(done==0)
             {
                  fgets(colName[0][colNameCount[0]], 30, fp);
                  if(colName[0][colNameCount[0]][0]=='@'){done=1;break;}
                  colNameCount[0]++;
             }
        }
        if(buffer[0]=='@'&&buffer[1]=='F'&&buffer[2]=='R')
        {
             while(done==0)
             {
                  fgets(colName[1][colNameCount[1]], 30, fp);
                  if(colName[1][colNameCount[1]][0]=='@'){done=1;break;}
                  colNameCount[1]++;
             }
        }
        if(buffer[0]=='@'&&buffer[1]=='S'&&buffer[2]=='P')
        {
             while(done==0)
             {
                  fgets(colName[2][colNameCount[2]], 30, fp);
                  if(colName[2][colNameCount[2]][0]=='@'){done=1;break;}
                  colNameCount[2]++;
             }
        }
        if(buffer[0]=='@'&&buffer[1]=='D'&&buffer[2]=='U')
        {
             while(done==0)
             {
                  fgets(colName[3][colNameCount[3]], 30, fp);
                  if(colName[3][colNameCount[3]][0]=='@'){done=1;break;}
                  colNameCount[3]++;
             }
        }        
    }
    fclose(fp);

    
//from initarrays1.cpp - end
    
    #ifdef DEBUG
    cout<<"    Loading Players {"<<endl;
    #endif
    InitPlayers();
    #ifdef DEBUG
    cout<<"    } Players"<<endl;
    #endif
    
    #ifdef DEBUG
    cout<<"  } GlobalData"<<endl;
    #endif
    return true; //global
}

bool GlobalData::InitNatives()
{
    //added 29/6; using new Indian class
    #ifdef DEBUG
    cout<<"    Loading Indian Data"<<endl;
    #endif
    indians = new Indians(m_pGame);
    return indians->Init();
}

bool GlobalData::InitPlayers()
{
    /*Player format
	    <country>
	  		    <name>England</name>
	  		    <color>255,0,0</color>
	  		    <nationality>English</nationality>
	  		    <abbrev>Eng.</abbrev>
	  		    <homeport>London</homeport>
	  		    <colonyname>New England</colonyname>
	  		    <independant>United States of America</independant>
	  		    <leader>
	  		             <name>Walter Raleigh</name>
	  		             <attitude> 1,-1, 0</attitude>
         	    </leader>
         	    <mission>Church of</mission>
	    </country>*/
	string tmp = "";
	
	TiXmlBase::SetCondenseWhiteSpace( false );
	
    #ifdef DEBUG
	cout<<"      Loading names"<<endl;
    #endif
    TiXmlDocument pFile("data/text/names.xml");
    
    bool loadOkay = pFile.LoadFile();
	
	if ( !loadOkay )
	{
		printf( "Could not load file 'names.xml'. Error='%s'. Exiting.\n", pFile.ErrorDesc() );
		exit( 1 );
	}
	
	TiXmlNode *node = 0;
	TiXmlNode *pNodeList = 0;
	
    #ifdef DEBUG
	cout<<"      Getting Names list"<<endl;
    #endif
	node = pFile.FirstChild("names");
	pNodeList = node->ToElement();
	
    #ifdef DEBUG
	cout<<"      Getting Countries {"<<endl;
    #endif
	node = pNodeList->FirstChild("country");
	//pNodeList = node->ToElement();
	
	while(node)
	{
	    Player *p = new Player();
	    if(!p->Init()) break;
	    
        #ifdef DEBUG
	    cout<<"        Set nodelist"<<endl;
        #endif
	    pNodeList = node->ToElement();	    

        #ifdef DEBUG
	    cout<<"        Creating Player "<<playerlist.size()<<" {"<<endl;
        #endif
	    p->setNationID(playerlist.size());
	    
        #ifdef DEBUG
	    cout<<"          Get Country Data {"<<endl;
        #endif
	    //Get Country Data
	    tmp = pNodeList->FirstChild("name")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->country.name = tmp;
	    tmp = pNodeList->FirstChild("color")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->Color->SetColor(tmp.c_str());
	    tmp = pNodeList->FirstChild("nationality")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->country.nationality = tmp;
	    tmp = pNodeList->FirstChild("abbrev")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->country.abbrev = tmp;
	    tmp = pNodeList->FirstChild("homeport")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->country.homeport = tmp;
        #ifdef DEBUG
	    cout<<"          } Country Data"<<endl;
        #endif
	    
        #ifdef DEBUG
	    cout<<"          Get Colony Data {"<<endl;
        #endif
	    //Get Colony Data
	    tmp = pNodeList->FirstChild("colonyname")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->NewWorld.name = tmp;
	    tmp = pNodeList->FirstChild("independent")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->NewWorld.independent = tmp;
	    tmp = pNodeList->FirstChild("mission")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"            Got "<<tmp<<endl;
        #endif
	    p->NewWorld.mission = tmp;
	    #ifdef DEBUG
	    cout<<"        } Colony Data"<<endl;
	    #endif
	    
        #ifdef DEBUG
	    cout<<"        Get Leader Data {"<<endl;
        #endif
	    //Get Leader Data
	    tmp = pNodeList->FirstChild("leaderName")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"          Got "<<tmp<<endl;
        #endif
	    p->setName(tmp.c_str());
	    tmp = pNodeList->FirstChild("leaderAttitude")->FirstChild()->Value();
        #ifdef DEBUG
	    cout<<"          Got "<<tmp<<endl;
        #endif
	    p->setAttitude(tmp.c_str());
	    #ifdef DEBUG
	    cout<<"        } Leader Data"<<endl;
	    #endif
	    
	    #ifdef DEBUG
	    cout<<"        Setting "<<p->getName()<<"'s gold"<<endl;
        #endif
        p->setGold(gold);
        
        #ifdef DEBUG
	    cout<<"        Push Player into the Players list"<<endl;
        #endif
	    playerlist.push_back(p);
	    
        #ifdef DEBUG
	    cout<<"      } Player"<<endl;
        #endif
	    node = pNodeList->NextSibling("country");
	}
	//TODO: where's the return?
	return true;
}

int GlobalData::getYear()
{
  return m_nYear;
}

void GlobalData::nextYear()
{
  m_nYear++;
}

//
// $Id$
//

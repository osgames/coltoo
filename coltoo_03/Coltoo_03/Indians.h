/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: Indians.h
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Indians
//
// DESCRIPTION	: class for indians.
//

#ifndef INDIANS_H
#define INDIANS_H

#include <vector>
using namespace std;

#include <SDL/SDL_types.h>

#include "gameEngine.h"
#include "Indian.h"
#include "message.h"
#include "tinyxml.h"

class IndianLevel;
class Indian;

class Indians
{
public:
//Constructor, destructor
    Indians();
    Indians(GameEngine*);
    virtual ~Indians();
    virtual bool Init(void);
    
    Indian *getIndian(int indianID);
    int getNumIndians();
    
    void LoadTribes(const char *fn);

private:
    void InitIndians(TiXmlNode *n);
    void InitIndianLevels(TiXmlNode *n);
    void CreateIndians();
    void CreateVillages();
    void CreateVillages(Indian *indian);
    
    GameEngine *m_pGame;
    vector<Indian*> indians;
    vector<IndianLevel*> indianLevels;
};

#endif

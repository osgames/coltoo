///////////////////////////////////////////////////////////
//  SurfacePrimitives.cpp
//  Implementation of the Class SurfacePrimitives
//  Created on:      16-Jul-2004 13:49:56
///////////////////////////////////////////////////////////

#include "SurfacePrimitives.h"

SurfacePrimitives::SurfacePrimitives(){

}


SurfacePrimitives::~SurfacePrimitives(){

}

void SurfacePrimitives::SetSurface(SDL_Surface *surface) {
  screen = surface;
}

SDL_Surface *SurfacePrimitives::GetSurface() {
  return screen;
}

void SurfacePrimitives::SetMask(SDL_Surface *surface) {
  mask = surface;
}

SDL_Surface *SurfacePrimitives::GetMask() {
  return mask;
}

void SurfacePrimitives::createSurface(SDL_Surface *base, int w, int h) {
	SDL_PixelFormat *form=base->format;
	screen=SDL_CreateRGBSurface(SDL_SWSURFACE,w,h,form->BitsPerPixel,
                              form->Rmask,form->Gmask,form->Bmask,form->Amask);
}


void SurfacePrimitives::fillSurface(int x, int y, int w, int h, ColorPrimitives * color) {
	Uint32 col;
  SDL_Rect dest;
  dest.x=x;dest.y=y;
  dest.w=w;dest.h=h;
	col = SDL_MapRGB(screen->format,color->Red(),color->Green(),color->Blue());
	SDL_FillRect(screen, &dest, col);
}


bool SurfacePrimitives::loadImage(const char* filename) {
  SDL_Surface *temp1 = NULL, *temp2 = NULL;
		
  temp1 = IMG_Load(filename);
  if (temp1) {
    temp2 = SDL_DisplayFormat(temp1);
    SDL_FreeSurface(temp1);
    return true;
  } else return false;
}


void SurfacePrimitives::drawTILE(SDL_Surface * from,
                                 int x, int y, int w, int h, int x2, int y2,
                                 ColorPrimitives * color) {
  SDL_Rect dest;
  dest.x=x;
  dest.y=y;
  SDL_Rect dest2;
  dest2.x=x2;
  dest2.y=y2;
  dest2.w=w;
  dest2.h=h;
  /*Next line sets the transparent color for the copying*/
  SDL_SetColorKey(from, SDL_SRCCOLORKEY,
                  SDL_MapRGB(from->format,color->Red(),
                             color->Green(),color->Blue()));
  SDL_BlitSurface(from,&dest2,screen,&dest);
}


void SurfacePrimitives::drawSURF(SDL_Surface * from,
                                 int x, int y,
                                ColorPrimitives * color) {
  SDL_Rect dest;
  dest.x = x;
  dest.y = y;
  SDL_SetColorKey(from, SDL_SRCCOLORKEY,
                  SDL_MapRGB(from->format,color->Red(),
                             color->Green(),color->Blue()));
  SDL_BlitSurface(from, NULL, screen, &dest);
}


Uint32 SurfacePrimitives::getPixel(SDL_Surface *from, int x, int y) {
  int bpp = from->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to retrieve */
  Uint8 *p = (Uint8 *)from->pixels + y * from->pitch + x * bpp;

  switch(bpp) {
    case 1:
      return *p;
    case 2:
      return *(Uint16 *)p;
    case 3:
      if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        return p[0] << 16 | p[1] << 8 | p[2];
      else
        return p[0] | p[1] << 8 | p[2] << 16;
    case 4:
      return *(Uint32 *)p;
    default:
      return 0;       /* shouldn't happen, but avoids warnings */
  }
}


Uint32 SurfacePrimitives::getPixel(int x, int y) {
  return getPixel(screen, x, y);
}


void SurfacePrimitives::putPixel(int x, int y, Uint32 pixel) {
  int bpp = screen->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to set */
  Uint8 *p = (Uint8 *)screen->pixels + y * screen->pitch + x * bpp;

  switch(bpp) {
    case 1:
      *p = pixel;
      break;
    case 2:
      *(Uint16 *)p = pixel;
      break;
    case 3:
      if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
        p[0] = (pixel >> 16) & 0xff;
        p[1] = (pixel >> 8) & 0xff;
        p[2] = pixel & 0xff;
      } else {
        p[0] = pixel & 0xff;
        p[1] = (pixel >> 8) & 0xff;
        p[2] = (pixel >> 16) & 0xff;
      }
      break;
    case 4:
      *(Uint32 *)p = pixel;
      break;
  }
}


bool SurfacePrimitives::createMask(SDL_Surface * from) {
	if(!from)return false;
	SDL_Surface *temp = screen;
	// Create new surface just like the old one.
	//New surface width and height modified by JDD.
	screen = SDL_CreateRGBSurface(SDL_SWSURFACE,1217,288,
                from->format->BitsPerPixel,from->format->Rmask,
                from->format->Gmask,from->format->Bmask,
                from->format->Amask);
	if(!screen)return false;
	
	//Ret is temporary storage, black and magicpink store the colors so we don't have to recalc them every pixel.
	Uint32 ret,black,magicpink;
	black = SDL_MapRGB(screen->format,0,0,0);
	magicpink = SDL_MapRGB(screen->format,255,0,255);

	// Lock the surface for direct pixel access (if needed)
	Slock();
	
	for(int q=0; q<screen->h; q++){  			// Rows
		for(int w=0; w<screen->w; w++){  		//Columns
			ret = getPixel(from, w, q); 
			if(ret == magicpink) putPixel(w, q, magicpink);
      else putPixel(w, q, black);
		}
	}
	
	Sulock();  // Unlock the surface
	mask = screen; screen = temp;
	return true;
}




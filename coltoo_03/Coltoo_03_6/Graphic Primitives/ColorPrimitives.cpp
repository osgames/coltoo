///////////////////////////////////////////////////////////
//  ColorPrimitives.cpp
//  Implementation of the Class ColorPrimitives
//  Created on:      16-Jul-2004 13:49:53
///////////////////////////////////////////////////////////

#include "ColorPrimitives.h"

ColorPrimitives::ColorPrimitives() {
  mvarRed = 0;
  mvarGreen = 0;
  mvarBlue = 0;
}

ColorPrimitives::ColorPrimitives(Uint32 red, Uint32 green, Uint32 blue) {
  setRed(red);
  setGreen(green);
  setBlue(blue);
}  


ColorPrimitives::~ColorPrimitives() {

}


Uint32 ColorPrimitives::Red() {
  return mvarRed;
}


Uint32 ColorPrimitives::Green() {
  return mvarGreen;
}


Uint32 ColorPrimitives::Blue() {
  return mvarBlue;
}


void ColorPrimitives::setRed(Uint32 red) {
  mvarRed = red;
}


void ColorPrimitives::setGreen(Uint32 green) {
  mvarGreen = green;
}


void ColorPrimitives::setBlue(Uint32 blue) {
  mvarBlue = blue;
}



///////////////////////////////////////////////////////////
//  LabelPrimitives.h
//  Implementation of the Class LabelPrimitives
//  Created on:      28-Jul-2004 18:24:53
///////////////////////////////////////////////////////////

#if !defined(LABELPRIMITIVES__INCLUDED_)
#define LABELPRIMITIVES__INCLUDED_

#include "WidgetPrimitives.h"
#include "WindowsPrimitives.h"

class LabelPrimitives : public WidgetPrimitives, public WindowsPrimitives {

public:
  LabelPrimitives();
	virtual ~LabelPrimitives();

public:
	FontPrimitives * getFont();
	void setFont(FontPrimitives * newVal);

private:
	FontPrimitives * font;

};

#endif // !defined(LABELPRIMITIVES__INCLUDED_)

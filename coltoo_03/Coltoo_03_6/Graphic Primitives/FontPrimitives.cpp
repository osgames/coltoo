///////////////////////////////////////////////////////////
//  FontPrimitives.cpp
//  Implementation of the Class FontPrimitives
//  Created on:      16-Jul-2004 13:49:54
///////////////////////////////////////////////////////////

#include "FontPrimitives.h"

FontPrimitives::FontPrimitives(){

}


FontPrimitives::~FontPrimitives(){

}


bool FontPrimitives::createFont(SDLFont font){

}


bool FontPrimitives::createFont(SDL_Surface * surface){

}


void FontPrimitives::drawString(SDL_Surface* screen, int x, int y, const char* format){

}


void FontPrimitives::drawShadowString(SDL_Surface* screen, int x, int y, bool shadow, const char* format){

}


void FontPrimitives::blitString(SDL_Surface* screen, int x, int y, const char* text){

}


void FontPrimitives::blitString(SDL_Surface* screen, int x, int y, const char* text, SDL_Color color){

}


void FontPrimitives::drawColorString(SDL_Surface * screen, ColorPrimitives * color1, ColorPrimitives * color2){

}


void FontPrimitives::setFontColor(ColorPrimitives * color1, ColorPrimitives * color2){

}


void FontPrimitives::initTrueType(){

}


void FontPrimitives::quitTrueType(){

}




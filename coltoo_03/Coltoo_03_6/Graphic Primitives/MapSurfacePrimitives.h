///////////////////////////////////////////////////////////
//  MapSurfacePrimitives.h
//  Implementation of the Class MapSurfacePrimitives
//  Created on:      16-Jul-2004 13:49:54
///////////////////////////////////////////////////////////

#if !defined(MAPSURFACEPRIMITIVES__INCLUDED_)
#define MAPSURFACEPRIMITIVES__INCLUDED_

#include "SurfacePrimitives.h"
#include "../Gamecore/Map.h"
#include "../Gamecore/Tile.h"
#include "../Gamecore/Actor.h"

class MapSurfacePrimitives : public SurfacePrimitives {

public:
	MapSurfacePrimitives();
	virtual ~MapSurfacePrimitives();
	bool createMap(Map * map);
	void drawTerrain();
	void drawOverlays();
	void drawShroud(Actor * player);
	void drawUnits();
	void drawRoad(int screenTile, long mapTile, int icon);
	void drawLinks(int screenTile, long mapTile, TileTypes tileType);

};

#endif // !defined(MAPSURFACEPRIMITIVES__INCLUDED_)

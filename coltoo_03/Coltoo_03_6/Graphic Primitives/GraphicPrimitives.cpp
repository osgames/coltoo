///////////////////////////////////////////////////////////
//  GraphicPrimitives.cpp
//  Implementation of the Class GraphicPrimitives
//  Created on:      16-Jul-2004 13:49:54
///////////////////////////////////////////////////////////

#include "GraphicPrimitives.h"
#include "ColorPrimitives.h"

GraphicPrimitives::GraphicPrimitives(){

}

GraphicPrimitives::~GraphicPrimitives(){

}

void GraphicPrimitives::SetScreen(SDL_Surface *surface) {
  screen = surface;
}

SDL_Surface *GraphicPrimitives::GetScreen() {
  return screen;
}

void GraphicPrimitives::Slock() {
  if(SDL_MUSTLOCK(screen)){if(SDL_LockSurface(screen)<0){return;}}
}

void GraphicPrimitives::Sulock() {
  if(SDL_MUSTLOCK(screen)){SDL_UnlockSurface(screen);}
}

void GraphicPrimitives::DrawPixel(int x, int y, ColorPrimitives *color) {
  Uint32 col = SDL_MapRGB(screen->format, color->Red(),
                          color->Green(), color->Blue());
  switch (screen->format->BytesPerPixel) {
    /* Assuming 8-bpp */  
    case 1: {
      Uint8 *bufp;
      bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
      *bufp = col;
    }
    break;
    /* Probably 15-bpp or 16-bpp */      
    case 2: {
      Uint16 *bufp;
      bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
      *bufp = col;
    }
    break;
    /* Slow 24-bpp mode, usually not used */      
    case 3: {
      Uint8 *bufp;
      bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
      if(SDL_BYTEORDER == SDL_LIL_ENDIAN) {
        bufp[0] = col;
        bufp[1] = col >> 8;
        bufp[2] = col >> 16;
      } else {
        bufp[2] = col;
        bufp[1] = col >> 8;
        bufp[0] = col >> 16;
      }
    }
    break;
    /* Probably 32-bpp */
    case 4: {
      Uint32 *bufp;
      bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
      *bufp = col;
    }
    break;
  }
}


void GraphicPrimitives::DrawLine(int x1, int y1, int x2, int y2,int flag,
                                 ColorPrimitives *color){
  int x,y;
  Slock();
  if(flag==1) {
    for(x=x1;x<x2+1;x++) {
      DrawPixel(x,y1,color);
    }
  } else {
    for(y=y1;y<y2+1;y++) {
      DrawPixel(x1,y,color);
    }
  }
  Sulock();
}

void GraphicPrimitives::DrawRect(int x, int y, int w, int h,
                                 ColorPrimitives *color) {
  int x1,y1;

  Slock();
  for(x1=x;x1<x+w+1;x1++) {
    DrawPixel(x1,y,color);
    DrawPixel(x1,y+h,color);
  }
  for(y1=y+1;y<y+h;y1++) {
    DrawPixel(x,y1,color);
    DrawPixel(x+w,y1,color);
  }
  Sulock();
}

void GraphicPrimitives::FillRect(int x, int y, int w, int h,
                                 ColorPrimitives *color){
	Uint32 col;
	SDL_Rect dest;
    dest.x = x; dest.y = y;
    dest.w = w; dest.h = h;
	col=SDL_MapRGB(screen->format, color->Red(), color->Green(), color->Blue());
	SDL_FillRect(screen, &dest, col);
}

void GraphicPrimitives::Border(int x, int y, int w, int h){
  ColorPrimitives *color = new ColorPrimitives(167, 116, 33);
  DrawLine(x-3,y-3,x+w+2,y-3,1,color);
  DrawLine(x-3,y-3,x-3,y+h+2,0,color);
  DrawLine(x-2,y-2,x+w+2,y-2,1,color);
  DrawLine(x-2,y-2,x-2,y+h+2,0,color);
  color = new ColorPrimitives(0, 0, 0);
  DrawLine(x-1,y-1,x+w+1,y-1,1,color);
  DrawLine(x-1,y-1,x-1,y+h+2,0,color);
  color = new ColorPrimitives(187, 116, 33);
  DrawLine(x-3,y+h+3,x+w+3,y+h+3,1,color);
  DrawLine(x+w+3,y-3,x+w+3,y+h+3,0,color);
  DrawLine(x-2,y+h+2,x+w+2,y+h+2,1,color);
  DrawLine(x+w+2,y-2,x+w+2,y+h+2,0,color);
  color = new ColorPrimitives(229, 200, 95);
  DrawLine(x-1,y+h+1,x+w+1,y+h+1,1,color);
  DrawLine(x+w+1,y-1,x+w+1,y+h+1,0,color);
}




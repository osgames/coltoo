///////////////////////////////////////////////////////////
//  ScreenPrimitives.h
//  Implementation of the Class ScreenPrimitives
//  Created on:      14-Jul-2004 19:54:38
///////////////////////////////////////////////////////////

#if !defined(SCREENPRIMITIVES__INCLUDED_)
#define SCREENPRIMITIVES__INCLUDED_

#include <vector>
#include "SurfacePrimitives.h"
#include "ButtonPrimitives.h"
#include "WindowsPrimitives.h"

using namespace std;

class ScreenPrimitives : public SurfacePrimitives {

public:
	ScreenPrimitives();
	virtual ~ScreenPrimitives();

public:
	void Splash();
	void RedrawScreen();
	void RedrawScreen(int x, int y);
	int getWidth();
	int getHeight();

protected:
	/**
	 * Contains all buttons created on screen
	 */
  vector<ButtonPrimitives*> buttons;
	/**
	 * Contains all windows created on screen. This includes all info windows (ex:
	 * gold display window, year display window, unit info window, etc) and all
	 * message windows (ex: popup MessageWindows)
	 */
  vector<WindowsPrimitives*> windows;
};

#endif // !defined(SCREENPRIMITIVES__INCLUDED_)

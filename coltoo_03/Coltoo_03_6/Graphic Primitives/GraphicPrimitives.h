///////////////////////////////////////////////////////////
//  GraphicPrimitives.h
//  Implementation of the Class GraphicPrimitives
//  Created on:      16-Jul-2004 13:49:54
///////////////////////////////////////////////////////////

#if !defined(GRAPHICPRIMITIVE__INCLUDED_)
#define GRAPHICPRIMITIVE__INCLUDED_

#include <SDL/SDL.h>
#include "ColorPrimitives.h"

using namespace std;

class GraphicPrimitives
{
private:
  SDL_Surface *screen;

public:
	GraphicPrimitives();
	virtual ~GraphicPrimitives();
	void SetScreen(SDL_Surface * surface);
	SDL_Surface *GetScreen();
	void Slock();
	void Sulock();
	void DrawPixel(int x, int y, ColorPrimitives *color);
	void DrawLine(int x1, int y1, int x2, int y2,int flag,
                ColorPrimitives *color);
	void DrawRect(int x, int y, int w, int h,
                ColorPrimitives *color);
	void FillRect(int x, int y, int w, int h, ColorPrimitives *color);
	void Border(int x, int y, int w, int h);
};




#endif // !defined(GRAPHICPRIMITIVE__INCLUDED_)

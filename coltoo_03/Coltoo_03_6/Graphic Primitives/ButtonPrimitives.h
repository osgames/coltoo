///////////////////////////////////////////////////////////
//  ButtonPrimitives.h
//  Implementation of the Class ButtonPrimitives
//  Created on:      16-Jul-2004 13:49:52
///////////////////////////////////////////////////////////

#if !defined(BUTTONPRIMITIVES__INCLUDED_)
#define BUTTONPRIMITIVES__INCLUDED_

#include "SurfacePrimitives.h"
#include "FontPrimitives.h"

class ButtonPrimitives : public SurfacePrimitives
{

public:
	ButtonPrimitives(const char *name, SDL_Surface * surface, SDLFont * font, int x, int y, int w, int h);
	virtual ~ButtonPrimitives();
	void createButton(const char *name, SDL_Surface * surface, SDLFont * font, int x, int y, int w, int h);
	void drawButton(int x, int y, int dx, int dy, const char* label);
	void pushButton(int x, int y, int push_x, int push_y, int dx, int dy, const char* label);
	void toggleButton();
	bool ButtonWasClicked(int mouse_x, int mouse_y);
	void setVisibility(bool visibility);
	bool IsVisible();

private:
  FontPrimitives *font;
};

#endif // !defined(BUTTONPRIMITIVES__INCLUDED_)

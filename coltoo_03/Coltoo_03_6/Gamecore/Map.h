///////////////////////////////////////////////////////////
//  Map.h
//  Implementation of the Class Map
//  Created on:      15-Jul-2004 0:01:31
///////////////////////////////////////////////////////////

#if !defined(MAP__INCLUDED_)
#define MAP__INCLUDED_

#include <vector>
#include "Tile.h"

using namespace std;

class Map {

public:
	Map();
	virtual ~Map();

private:
  vector<Tile*> tiles;
};


#endif // !defined(MAP__INCLUDED_)

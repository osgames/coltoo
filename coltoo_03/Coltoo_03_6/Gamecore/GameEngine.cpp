///////////////////////////////////////////////////////////
//  GameEngine.cpp
//  Implementation of the Class GameEngine
//  Created on:      15-Jul-2004 0:30:02
///////////////////////////////////////////////////////////

#include "GameEngine.h"


GameEngine::GameEngine() {
  localGE = true;
  graph = new GraphicEngine();
}


GameEngine::GameEngine(GraphicEngine *graphics) : graph(graphics) {
  localGE = false;
}


GameEngine::~GameEngine(){
  if(localGE) delete graph;
}


bool GameEngine::Init(int argc, char **argv) {
  if(!graph) return false;
}


void GameEngine::GameLoop() {
  
}

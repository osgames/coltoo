///////////////////////////////////////////////////////////
//  AISubsystem.h
//  Implementation of the Class AISubsystem
//  Created on:      15-Jul-2004 0:28:03
///////////////////////////////////////////////////////////

#if !defined(AISUBSYSTEM__INCLUDED_)
#define AISUBSYSTEM__INCLUDED_

#include "InputHandler.h"

class AISubsystem : public InputHandler
{

public:
	AISubsystem();
	virtual ~AISubsystem();

};

#endif // !defined(AISUBSYSTEM__INCLUDED_)

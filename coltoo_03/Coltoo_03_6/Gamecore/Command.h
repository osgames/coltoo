///////////////////////////////////////////////////////////
//  Command.h
//  Implementation of the Class Command
//  Created on:      15-Jul-2004 0:24:20
///////////////////////////////////////////////////////////

#if !defined(COMMAND__INCLUDED_)
#define COMMAND__INCLUDED_

class Command {

public:
	Command();
	virtual ~Command();

};

#endif // !defined(COMMAND__INCLUDED_)

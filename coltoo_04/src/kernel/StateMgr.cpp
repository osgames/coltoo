/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "StateMgr.h"

StateMgr::StateMgr()
{
}

StateMgr::~StateMgr()
{
    popAll();
}

void StateMgr::changeState(MPState aState)
{
    if (!gameStates.empty())
    {
        gameStates.top()->end();
        gameStates.pop();
    }
    
    gameStates.push(aState);
    gameStates.top()->init();       
}

void StateMgr::pushState(MPState aState)
{
    if (!gameStates.empty())
    {
        gameStates.top()->pause();
    }
    
    gameStates.push(aState);
    gameStates.top()->init();  
}


void StateMgr::popState()
{
    if (!gameStates.empty())
    {
        gameStates.top()->end();
        gameStates.pop();
    } 
    
    if(!gameStates.empty())
    {
        gameStates.top()->resume();
    }
}

void StateMgr::popAll()
{
    //No explicit delete of objects on stack because 
    //ManagedPointer & ManagedObject classes take care of that
    
    std::cout << "Popping remaining states (" << countStates() << ")" 
              << std::endl;
              
    while (!gameStates.empty())
    {
        gameStates.pop();
    }
}

void StateMgr::input()
{
    gameStates.top()->input();
}

void StateMgr::update()
{
    gameStates.top()->update();
}

void StateMgr::draw()
{
    gameStates.top()->draw();
}

unsigned long StateMgr::countStates() const
{
    return gameStates.size();
}

void StateMgr::printStack()
{
    stack<MPState> tmp;
    MPState s;
    
    std::cout << "\nGame states (" << countStates() << ")\n";
    while (!gameStates.empty())
    {
        s = gameStates.top();
        std::cout << "\t" << s->identify() << "\n";
        tmp.push(s);
        gameStates.pop();
    }
    std::cout << std::endl;
    
    while (!tmp.empty())
    {
        gameStates.push(tmp.top());
        tmp.pop();
    }    
}

/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "MainMenuState.h"

MainMenuState::MainMenuState()
{
}

MainMenuState::~MainMenuState()
{
}

void MainMenuState::init()
{
    std::cout << "MainMenuState init" << std::endl;
    
    GraphicsEngine::get().splash();
    
    //Jump to game
    new Universe();
    StateMgr::get().pushState(new MapPlayState());
}

void MainMenuState::pause()
{
    std::cout << "MainMenuState pause" << std::endl;
}

void MainMenuState::resume()
{
    std::cout << "MainMenuState resume" << std::endl;

    //Skipping main menu for now
    Application::get().quit();
}

void MainMenuState::end()
{
    std::cout << "MainMenuState end" << std::endl;

//    StateMgr::get().popState(); //MapPlayState
    delete Universe::getPtr();  
}

void MainMenuState::input()
{
    std::cout << "MainMenuState input" << std::endl;
}

void MainMenuState::update()
{
    std::cout << "MainMenuState update" << std::endl;
}

void MainMenuState::draw()
{
    std::cout << "MainMenuState draw" << std::endl;
}

string MainMenuState::identify()
{
    return "MainMenu";
}


/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "Universe.h"

Universe::Universe()
{
    init();
}

Universe::~Universe()
{
    end();
}

void Universe::init()
{
    std::cout << "Big Bang" << std::endl;
    
    //Set up map
    /* TODO (#1#): Choose map at random */
    map = new Map("data/text/map08.txt");
    
    //Set up actors
    actors.push_back(new Player());  
    
    /* Ignoring AI for now
    for (int i=0; i<=5; ++i)
        actors.push_back(new AI());
    */
        
    currActor = *(actors.begin());   
}

void Universe::end()
{
    std::cout << "Big Crunch" << std::endl;
    
    actors.erase(actors.begin(), actors.end());
      
    //ManagedPointer cleans up other members
}

void Universe::nextActor()
{
    ActorIt it = find(actors.begin(), actors.end(), currActor);
    
    if (*it == actors.back())
        currActor = *(actors.begin());
    else
        currActor = *(++it);
}

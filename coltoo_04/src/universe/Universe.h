/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Game universe.
    Holds game elements such as the world map and actors.
*/

#ifndef UNIVERSE_H
#define UNIVERSE_H

#include <iostream>

#include <list>
using std::list;

#include "memMgr/Singleton.h"
#include "actors/Actor.h"
#include "actors/Player.h"
#include "actors/AI.h"

#include "Map.h"

class Universe : public Singleton<Universe>
{
	public:
		Universe();
		~Universe();
		
		void init();
		void end();
		
		void nextActor();
	
	private:
	    list<MPActor> actors;
	    MPActor currActor;
	    
	    MPMap map;	    
};

typedef list<MPActor>::iterator ActorIt;
#endif // UNIVERSE_H

